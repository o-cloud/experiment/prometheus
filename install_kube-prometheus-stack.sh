#!/bin/sh
kubectl create namespace monitoring

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

helm install -n monitoring prometheus prometheus-community/kube-prometheus-stack -f values_kube-prometheus-stack.yaml

sleep 30
