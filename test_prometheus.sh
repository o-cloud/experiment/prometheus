#!/bin/zsh
#
# Create volume claim
kubectl apply -f pvc-demo.yaml

sleep 5

# Start jobs
ns=workflow
kubectl create namespace $ns

for i in $(seq 1 2)
do
  cat <<EOF | kubectl -n $ns apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: global-$i
  labels: 
    application: prometheus
spec:
  template:
    metadata:
      labels:
        version: dev
    spec:
      volumes:
        - name: task-pv-storage
          persistentVolumeClaim:
            claimName: pvc-demo
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "cat /dev/zero > /dev/null"]
        volumeMounts:
          - mountPath: "/mnt/data/"
            name: task-pv-storage
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
EOF
done
