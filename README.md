# Prometheus

## Todo
* Check resolution time for kube_state_metrics
* Check GCP, AWS pricing (memory, cpu, network, etc.)
* Understand the metrics we retrieve (memory ?, cpu ?)
* Check the needs regarding the database storage, size, behavior in case of crash, etc.


Benchmark on Prometheus to collect metric and establish the cost of the workflows.

## Installing Prometheus
Prometheus can be installed via Helm. You can use the `install_prometheus.sh` script to set your helm repository and install prometheus. 
```bash
sh install_kube-prometheus-stack.sh;
kubectl port-forward --namespace monitoring svc/prometheus-kube-prometheus-prometheus 9090:9090
# If grafana is installed
kubectl port-forward --namespace monitoring service/prometheus-grafana 9091:80
```
**NOTE** you can set the `grafana; enabled` variable to true if you need to use grafana or to false in order not to install grafana.

Here are the consumption of each pods launched with prometheus
```bash
kubectl top pod -n monitoring
NAME                                                     CPU(cores)   MEMORY(bytes)
alertmanager-prometheus-kube-prometheus-alertmanager-0   1m           15Mi
prometheus-grafana-5cddc775c4-hvghh                      1m           106Mi
prometheus-kube-prometheus-operator-86d499db8f-spwkh     1m           37Mi
prometheus-kube-state-metrics-788f8c676b-tngtl           3m           12Mi
prometheus-prometheus-kube-prometheus-prometheus-0       140m         362Mi
prometheus-prometheus-node-exporter-cvght                5m           9Mi
prometheus-prometheus-node-exporter-grf5w                1m           10Mi
prometheus-prometheus-node-exporter-hxlt5                1m           8Mi
prometheus-prometheus-node-exporter-mplh5                1m           8Mi
prometheus-prometheus-node-exporter-phzrc                3m           8Mi
```

Since version `2.0.0` prometheus does not retrieve labels by default ([see here](https://github.com/kubernetes/kube-state-metrics/issues/1501)). Hence, you have to add the `- --metric-labels-allowlist=pods=[*]` to retrive pods labels. This is added in the `values.yaml` file that is used for the installation of prometheus

## Exporters
Exporters allow to convert a metric in something that prometheus can understand. By default two exporters are deployed, i.e., `kube_state_metrics` and `node_exporter`.

### Kube-state-metrics
`Kube-state-metrics` "generates Prometheus format metrics based on the current state of the Kubernetes native resources".
It seems that the scrape interval is 15s. Not sure this value can be modified.
You can find the description of each metrics in the [project documentation](https://github.com/kubernetes/kube-state-metrics/blob/master/docs).

### Node exporter
"The Prometheus Node Exporter exposes a wide variety of hardware- and kernel-related metrics."
More information on the [documentation page](https://prometheus.io/docs/guides/node-exporter/), on the [git repo](https://github.com/prometheus/node_exporter) and
[metric description](https://github.com/prometheus/node_exporter/blob/9def2f9222d61babbcaeb95a407d1558601cb4d1/collector/fixtures/e2e-64k-page-output.txt).

### cAdvisor
"cAdvisor (short for container Advisor) analyzes and exposes resource usage and performance data from running containers. cAdvisor exposes Prometheus metrics out of the box."
Check [here](https://github.com/google/cadvisor/blob/85796f19bf1986040f2b9644857fd46811b737f6/docs/storage/prometheus.md) for the description of the metrics provided by `cAdvisor`, 
[here](https://prometheus.io/docs/guides/cadvisor/) for the documentation and [here](https://github.com/google/cadvisor) for the git repo.

The exporter gives the general metrics for the `pvc`, but no pod-related metrics.


## Persistence of Prometheus data
In order to be resilient to a crash of the prometheus server, we store the prometheus database on a `PersistentVolume`.
This is achieved by overloading the `values.yaml` with the following
```yaml
prometheus:
  prometheusSpec:
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: standard
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 50Gi
```

## Metrics extraction
| Metric | Query | Comment |
| -----  | ----- | ------- |
| Retrieve pod creation time | `kube_pod_created{namespace="NAMESPACE", pod="PODNAME"}` | |
| Retrieve CPU request | sum(cluster:namespace:pod_cpu:active:kube_pod_container_resource_requests{cluster="", namespace="workflow"}) by (pod) | Extracted from grafana |
| Retrieve CPU usage | `sum(rate(container_cpu_usage_seconds_total{container="c"}[5m])) by (pod)` | Averaged CPU usage over 5 minutes. Check if that is the right metric to use ! |
| Retrieve CPU usage | `sum(rate(container_cpu_usage_seconds_total{container="c"}[5m])) by (pod)` | Extracted from grafan. Maybe the right one to use because of [this](https://prometheus.io/docs/prometheus/latest/querying/functions/#irate) |
| Retrieve CPU usage over time rage | `http://localhost:9090/api/v1/query_range?query=sum(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{cluster="",+namespace="workflow", pod="global-1-qx4c5"})&start=1635928120&end=1635934500    &step=10`| Extracted from grafan, to be used in postman |
| Memory usage | `sum(container_memory_working_set_bytes{cluster="", namespace="workflow", container!="", image!=""}) by (pod)`| Extracted from grafana |
| Persistent Volum Claim usage | `( sum without(instance, node) (topk(1, (kubelet_volume_stats_capacity_bytes{cluster="", job="kubelet", metrics_path="/metrics", namespace="workflow", persistentvolumeclaim="pvc-demo"}))) - sum without(instance, node) (topk(1, (kubelet_volume_stats_available_bytes{cluster="", job="kubelet", metrics_path="/metrics", namespace="workflow", persistentvolumeclaim="pvc-demo"}))))`| Space used on PVC. Set the `namespace` and `pvc` name. |
| Network byte received | `container_network_receive_bytes_total{cluster="", namespace=~"workflow"}` | Network byte received. Set the `namespace` and pod. | 
| Network byte transmitted | `container_network_transmit_bytes_total{cluster="", namespace=~".+"}` | Network byte transmitter. Set the `namespace` and pod. | 
* No exporter has been found, yet, to get the usage of a `pvc` by pod.

# Cost estimation
We first plan to use a simple cost estimation based on the average of the metrics over the running
period, weighted by the rate of that metric. 

$`P_{M}(t) = (\sum_{i=1}^{n} \frac{M_i}{n}) \times t(hour) \times Rate({hour}^{-1})`$

* $`P_{M}(t)`$ is the total cost of a metric for a running time $`t`$
* $`M`$ is the metric to evaluate
* $`M_i`$ is the value of the $i^{th}$ metric point
* $`n`$ is the total number of metric points
* $`T(hour)`$ is the time the workflow has been running in hour
* $`Rate(hour)`$ is the rate of the metric on an hour basis

For example, let's assume we obtain the averaged cpu consumption of 2.5 vCPU for a workflow running 30 minutes with a rate of $\$0.021811 /(vCPU\space hour)$
The cost of this consumption is then:

$`P(t) = 2.5 \times 0.5 \times 0.021811 = 0.02726375`$

## Rates
We could distinguish 3 main metrics that are used to establish the price of the cluster, according to the [gpc pricing](https://cloud.google.com/compute/all-pricing#high_bandwidth_configuration)
* [CPU usage](https://cloud.google.com/compute/all-pricing#general_purpose)
* [Memory](https://cloud.google.com/compute/all-pricing#general_purpose)
* [Network bandwith for machines with higher bandwith](https://cloud.google.com/compute/all-pricing#high_bandwidth_configuration)
